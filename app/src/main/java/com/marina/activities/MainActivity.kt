package com.marina.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.marina.activities.InfoActivity.Companion.PERSON
import com.marina.activities.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnGoToThirdScreen.setOnClickListener {
            if (validateTextFields()) {
                goToThirdActivity()
            }
        }
    }

    //проверяем, что обязательные поля не пустые
    private fun validateTextFields(): Boolean {
        val name: String = binding.nameInputText.text.toString()
        val surname: String = binding.surnameInputText.text.toString()
        val patronymic: String = binding.fatherNameInputText.text.toString()
        val age: Int? = binding.ageInputText.text.toString().toIntOrNull()

        if (name.isEmpty() || surname.isEmpty() || patronymic.isEmpty() || age == null) {
            Toast.makeText(this, getString(R.string.fill_all_fields), Toast.LENGTH_SHORT).show()
            return false
        }
        return true
    }

    //создаем интент и стартуем активити
    private fun goToThirdActivity() {
        val name: String = binding.nameInputText.text.toString()
        val surname: String = binding.surnameInputText.text.toString()
        val patronymic: String = binding.fatherNameInputText.text.toString()
        val age: Int = binding.ageInputText.text.toString().toInt()
        val hobby: String = binding.hobbyInputText.text.toString()

        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra(
            PERSON,
            PersonInfo(
                name = name,
                surname = surname,
                patronymic = patronymic,
                age = age,
                hobby = hobby
            )
        )
        startActivity(intent)
    }
}
