package com.marina.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.marina.activities.databinding.ActivityInfoBinding

class InfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setText()
        binding.btnGoToThirdScreen.setOnClickListener {
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setText() {
        val person = intent.getParcelableExtra<PersonInfo>(PERSON)

        with(binding) {
            if (person?.age!! > 30) {
                infoNameText.text = "${person.surname} ${person.name} ${person.patronymic}"
            } else {
                infoNameText.text = "${person.surname} ${person.name}"
            }
            infoAgeText.text = person.age.toString()
            if (person.hobby.isNotEmpty()) {
                yourHobbyText.visibility = View.VISIBLE
                infoHobbyText.text = person.hobby
            }
        }
    }

    companion object {
        const val PERSON = "person"
    }
}

